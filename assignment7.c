//assignment7a.c

#include<stdio.h>
int main()
{   
	int a[10], i, n, key, found = 0;
	printf("Enter the number of elements ");
	scanf("%d", &n);
	printf("Enter %d elements \n",n );
	for (i = 0; i < n; i++)
	{
		scanf("%d", &a[i]);
	}
	printf("Enter the element to be searched ");
	scanf("%d", &key);
	for (i = 0; i < n ; i++)
	{
		if (a[i] ==key)
		{
			found = 1;
			break;
		}
	}
	if (found == 1)
		printf("Successful Search, element is present in the array at position %d",i+1);
	else
		printf("Unsucessful Search, element is not present in the array\n");
	return 0;
}


//assignment7b.c

#include<stdio.h>
int main()
{
	int a[50],n,i;
	float avg,sum=0;
	printf("Enter the total number of elements \n");
	scanf("%d",&n);
	printf("Enter the numbers to find its average \n");
	for (i = 0; i < n; i++)
	{
		scanf("%d", &a[i]);
	}
	for (i = 0; i < n; i++)
	{
		sum = sum +a[i];
	}
	avg=sum/n;
	printf("Average = %f\n",avg);
	return 0;
}

//assignment7c.c

#include <stdio.h>
int main()
{
	int arr[100], i, j, n, Count = 0;
	printf(" Please Enter Number of elements in an array  :   ");
	scanf("%d", &n);
	printf(" Please Enter %d elements of an Array  :  ", n);
	for (i = 0; i < n; i++)
	{
		scanf("%d", &arr[i]);
	}     
	for (i = 0; i < n; i++)
	{
		for(j = i + 1; j < n; j++)
		{
			if(arr[i] == arr[j])
			{
				Count++;
				break;
			}
		}
	}
	printf(" Total Number of Duplicate Elements in this Array  =  %d ", Count);
	return 0;
}