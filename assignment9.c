9.a:
#include <stdio.h>
struct student {
	char firstName[50];
	int roll;
	float marks;
} s[10];
int main() 
{
	int i;
	printf("Enter information of students:\n");
	for (i = 0; i < 10; ++i) 
	{
		printf("Enter roll no. : ");
		scanf("%d",s[i].roll);
		printf("Enter first name: ");
		scanf("%s", s[i].firstName);
		printf("Enter marks: ");
		scanf("%f", &s[i].marks);
	}
	printf("Displaying Information:\n");
	for (i = 0; i < 10; ++i) 
	{
		printf("Roll number: %d\n",s[i].roll);
		printf("First name: ");
		puts(s[i].firstName);
		printf("Marks: %.1f", s[i].marks);
		printf("\n");
	}
	return 0;
}

9.b:

#include <studio.h>
struct Name
{
	char book[20];
	char author[20];
};
struct bookdetail
{
	struct Name name;
	int pages;
	float price;
};
void output(struct bookdetail v[],int n)
{
	int i,t=1;
	for(i=0;i<n;i++,t++)
	{
		printf("\n");
		printf("Book No.%d\n",t);
		printf("Book %d Name is=%s \n",t,v[i].name.book);
		printf("Book %d Author is=%s \n",t,v[i].name.author);
		printf("Book %d Pages is=%d \n",t,v[i].pages);
		printf("Book %d Price is=%f \n",t,v[i].price);
		printf("\n");
	}
}
void main()
{
	struct bookdetail b[20];
	int num,i;
	printf("Enter the Numbers of Books:");
	scanf("%d",&num);
	printf("\n");
	for(i=0;i<num;i++)
	{
		printf("Book %d Detail",i+1);
		printf("\nEnter the Book Name:\n");
		scanf("%s",b[i].name.book);
		printf("Enter the Author of Book:\n");
		scanf("%s",b[i].name.author);
		printf("Enter the Pages of Book:\n");
		scanf("%d",&b[i].pages);
		printf("Enter the Price of Book:\n");
		scanf("%f",&b[i].price);
	}
	output(b,num);
}
